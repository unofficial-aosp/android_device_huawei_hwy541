#
# Copyright 2013 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

DEVICE_FOLDER := device/huawei/hwy541

#ifeq ($(TARGET_PREBUILT_KERNEL),)
#LOCAL_KERNEL := device/huawei/hwy541-kernel/kernel
#else
#LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
#endif

#PRODUCT_COPY_FILES := \
#    $(LOCAL_KERNEL):kernel

# Touchscreen
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/touchscreen/focaltech_ts.idc:system/usr/idc/focaltech_ts.idc

# Permissions
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/permissions/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml

# Audio configuration
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/audio/audio_policy.conf:system/etc/audio_policy.conf \
    $(DEVICE_FOLDER)/audio/audio_hw.xml:system/etc/audio_hw.xml \
    $(DEVICE_FOLDER)/audio/audio_para:system/etc/audio_para \
    $(DEVICE_FOLDER)/audio/tiny_hw.xml:system/etc/tiny_hw.xml

# Keylayout
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/keylayout/headset-keyboard.kl:system/usr/keylayout/headset-keyboard.kl \
    $(DEVICE_FOLDER)/keylayout/sci-keypad.kl:system/usr/keylayout/sci-keypad.kl \
    $(DEVICE_FOLDER)/keylayout/gpio-keys.kl:system/usr/keylayout/gpio-keys.kl

# Wifi configuration
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/wifi/bcmdhd.cal:system/etc/wifi/bcmdhd.cal \
    $(DEVICE_FOLDER)/wifi/wpa_supplicant_overlay.conf:system/etc/wifi/wpa_supplicant_overlay.conf

# Bluetooth confirguration
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/bluetooth/bt_stack.conf:system/etc/bluetooth/bt_stack.conf \
    $(DEVICE_FOLDER)/bluetooth/bt_vendor.conf:system/etc/bluetooth/bt_vendor.conf

# Media profiles
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/media_profiles.xml:system/etc/media_profiles.xml \
    $(DEVICE_FOLDER)/media_codecs.xml:system/etc/media_codecs.xml \
    $(DEVICE_FOLDER)/codec_pga.xml:system/etc/codec_pga.xml

PRODUCT_PACKAGES += \
    sdcard_wrapper \
    libaudio-resampler \
    libhwui \
    libtinyalsa \
    libtinyxml \

PRODUCT_AAPT_CONFIG := hdpi

DEVICE_PACKAGE_OVERLAYS := \
   $(DEVICE_FOLDER)/overlay

PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.android.dataroaming=false \
    persist.msms.phone_default=0 \
    persist.sys.modem.diag=,none \
    persist.sys.slog=0 \
    sys.usb.gser.count=4 \
    lmk.autocalc=false \
    ro.floatkey.show=false \
    ro.msms.phone_count=2 \
    persist.msms.phone_count=2 \
    ro.modem.w.count=2 \
    persist.sys.sprd.modemreset=1 \
    persist.sys.sprd.wcnreset=1 \
    ro.storage.flash_type=2 \
    ro.dual.sim.phone=true \
    persist.sys.usb.config=mtp,adb \
    persist.service.adb.enable=1 \
    persist.service.debuggable=1

# Dalvik
PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.heapstartsize=8m \
    dalvik.vm.heapgrowthlimit=96m \
    dalvik.vm.heapsize=256m \
    dalvik.vm.heaptargetutilization=0.75 \
    dalvik.vm.heapminfree=512k \
    dalvik.vm.heapmaxfree=8m

# Flashfire
$(call inherit-product-if-exists, vendor/chainfire/flashfire/chainfire-vendor.mk)
