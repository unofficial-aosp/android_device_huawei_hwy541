LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := sdcard.c
LOCAL_MODULE := sdcard_wrapper
LOCAL_CFLAGS := -Wall -Wno-unused-parameter

LOCAL_SHARED_LIBRARIES := libc libcutils

LOCAL_POST_INSTALL_CMD := \
    $(hide) mkdir -p $(TARGET_OUT_EXECUTABLES); \
    ln -sf $(LOCAL_MODULE) $(TARGET_OUT_EXECUTABLES)/sdcard \

include $(BUILD_EXECUTABLE)
