DEVICE_FOLDER := device/huawei/hwy541

# Permissions
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/permissions/platform.xml:system/etc/permissions/platform.xml

# Wifi configuration
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/wifi/wpa_supplicant.conf:system/etc/wifi/wpa_supplicant.conf

# Proprietaries
$(call inherit-product-if-exists, vendor/huawei/hwy541/hwy541-vendor.mk)
